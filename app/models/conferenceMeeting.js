const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const MeetingSchema = new Schema({
    meetingCode: { type: String, unique: true },
    name: { type: Object, require: true },
    host: { type: Object, require: true },
    participants: { type: Object, require: true },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

MeetingSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});


const Meeting = mongoose.model('meetings', MeetingSchema, 'meetings');

module.exports = Meeting;