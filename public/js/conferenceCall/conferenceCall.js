var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var meetingCode = query('meetingCode') ? query('meetingCode') : null;

var roomName = meetingCode.split('_');
roomName = roomName[0];

$('.meeting-name').html(`<h4 class="text-muted" id="roomName">${roomName}</h4>`)

var username = $('body').attr("data-username");
var isHost;
var recordFlag = true;
var screenShareFlag = false;
var raiseHandFlag = false;

$(document).ready(function () {

    isHost = sessionStorage.getItem('flag')
        ? 'isHostTrue' == atob(sessionStorage.getItem('flag'))
            ? true : false : false;

    console.log("\n isHost > " + isHost);

    if (isHost) {

        $(`#players`).empty();
        $(`#hostVideoContainer`).append(`<video class="host-video-player" id="localVideo" autoplay muted playsinline></video><div class="host-name">${username}</div>`);
        initialise_webRTCAdaptor(false, autoRepublishEnabled);

        $(`#extra-action-options`).empty();
        $(`#extra-action-options`).append(`<a href="#" class="dropdown-item screen-share-btn" id="screen-share-btn">Present now</a><a href="#" class="dropdown-item vod-record-btn" id="vod-record-btn">Start record</a>`);

    } else {
        initialise_webRTCAdaptor(false, autoRepublishEnabled);

        $(`#extra-action-options`).empty();
        $(`#extra-action-options`).append(`<a href="#" class="dropdown-item raise-hand-btn" id="raise-hand-btn">Raise hand</a>`);

    }

    // select2 plugin initialisation
    $("#choose_people").select2({
        tags: true,
        tokenSeparators: [",", " "]
    });


    // select mail id and send meeting invation mail
    $('#addPeopleForm').submit(function (e) {
        e.preventDefault();

        var params = {
            selected_peoples: $("#choose_people").val(),
            roomName: roomName,
            meetingCode: meetingCode,
        }
        $.post('/v1/video/sendInvitationMail',
            params,
            function (data, status) {
                if (data.status === 200) {
                    console.log(data.message);
                    toastr.options = { "positionClass": "toast-bottom-left" };
                    toastr.info('Invite sent');
                    $('#add-people-modal').modal('toggle');
                    $('#choose_people').val(null).trigger('change');
                } else {
                    console.log(data.message);
                }
            })
    })


    // chat form
    // $("#chat-form").submit(function (event) {
    //     event.preventDefault();
    //     const message = $('#chat-message').val().trim()
    //     appendMessage(`<span class="text-info">You : </span><span class="text-white">${message}</span>`)
    //     sendNotificationEvent('MESSAGE', username, message);
    //     $('#chat-message').val('');
    // })


    // emoji plugin initialisation
    // and chat form submission
    $("#chat-message").emojioneArea({
        pickerPosition: "top",
        search: false,
        events: {
            keyup: function (editor, event) {
                if (event.which == 13) {
                    if (event.shiftKey) {
                        // With shift
                    } else {
                        event.preventDefault();
                        const message = $('#chat-message').data("emojioneArea").getText().trim();
                        if (message !== '') {
                            appendMessage(`<span class="text-info">You : </span><span class="text-white">${message}</span>`);
                            sendNotificationEvent('MESSAGE', username, message);
                            $('#chat-message').data("emojioneArea").setText('');
                            $("#chat-message").data("emojioneArea").hidePicker();
                        }
                    }
                }
            }
        }
    });

})


// display message
function appendMessage(message) {

    $('#chat-div').append(`<div class="message-span-div">${message}</div>`)

    // to keep scroll to the bottom of the div
    var objDiv = document.getElementById("chat-div");
    objDiv.scrollTop = objDiv.scrollHeight;

}


// remote microphone button click event
$(document).on("click", ".remote-microphone-btn", function (e) {
    if ($(this).hasClass("active")) {
        toastr.options = { "positionClass": "toast-bottom-left" };
        toastr.info(`you can't unmute someone else.`);
    } else {
        let id = $(this).parent().attr("data-streamId");
        sendNotificationEvent("REMOTE_MIC_MUTED", username, '', id);
    }
});


//remote camera button click event
$(document).on("click", ".remote-camera-btn", function (e) {
    if ($(this).hasClass("active")) {
        toastr.options = { "positionClass": "toast-bottom-left" };
        toastr.info(`you can't.`);
    } else {
        let id = $(this).parent().attr("data-streamId");
        sendNotificationEvent("REMOTE_CAM_TURNED_OFF", username, '', id);
    }
});


$(document).on("click", ".screen-share-btn", function (e) {

    var screenShareBtnId = document.getElementById("screen-share-btn");

    if (screenShareFlag) {
        webRTCAdaptor.switchVideoCameraCapture(publishStreamId, 'camera');
        $(`.host-video-player`).css({ 'transform': 'scaleX(-1)', '-webkit-transform': 'scaleX(-1)' });
        $(`.host-video-player`).removeClass('force-hidden');
        $(`.screen-share-active-screen`).addClass('force-hidden');
        if (isCameraOff) {
            turnOnLocalCamera();
        }
        sendNotificationEvent('SCREEN_SHARE_STOPPED');
        screenShareBtnId.innerHTML = "Present now";
        screenShareFlag = false;
    } else {
        webRTCAdaptor.switchDesktopCapture(publishStreamId);
        $(`.host-video-player`).css({ 'transform': 'none', '-webkit-transform': 'none' });
        $(`.host-video-player`).addClass('force-hidden');
        $(`.screen-share-active-screen`).removeClass('force-hidden');
        sendNotificationEvent('SCREEN_SHARE_STARTED');
        screenShareBtnId.innerHTML = "Stop presenting";
        screenShareFlag = true;
    }

})


$(document).on("click", ".vod-record-btn", function (e) {

    var hostStreamId = $('.host-video-player').attr("data-hostStreamId");
    var vodRecordBtnId = document.getElementById("vod-record-btn");

    var params = {
        streamId: hostStreamId,
        recordingStatus: recordFlag
    }

    $.post('/v1/video/VoDRecord',
        params,
        function (data, status) {
            if (data.status === 200) {

                // console.log("\n \n JOSN > " + JSON.stringify(data))
                // console.log(data.message);

                if (recordFlag) {
                    $(`#recordingInfo`).removeClass(`hidden`);
                    vodRecordBtnId.innerHTML = "Stop record";
                    recordFlag = false;
                } else {
                    $(`#recordingInfo`).addClass(`hidden`);
                    vodRecordBtnId.innerHTML = "Start record";
                    recordFlag = true;
                }

            } else {
                console.log(data.message);
            }
        })
})


$(document).on("click", ".raise-hand-btn", function (e) {

    var raiseHandBtnId = document.getElementById("raise-hand-btn");

    if (raiseHandFlag) {
        sendNotificationEvent('HAND_DOWN');
        raiseHandBtnId.innerHTML = "Raise hand";
        raiseHandFlag = false;
    } else {
        sendNotificationEvent('HAND_UP');
        raiseHandBtnId.innerHTML = "Lower hand";
        raiseHandFlag = true;
    }
})


// getUsers
function getUsers() {
    $.get('/v1/cognito/getUsers', function (data, status) {
        if (data.status == 200) {
            //console.log("getUsers >> : " + JSON.stringify(data.data))
            var users = data.data.Users;
            var user_option = ``;
            users.forEach(function (element, i) {

                user_option += `<option value="${element.Username}">${element.Attributes[0].Value}</option>`;

                if (i == users.length - 1) {
                    $('#user-list').html(user_option)
                }
            });
        }
    });
}
getUsers();


async function addParticipant(streamId) {
    var params = {
        streamId: streamId,
        meetingCode: meetingCode
    }
    $.post('/v1/video/addParticipant',
        params,
        function (data, status) {
            if (data.status === 200) {
                console.log(data.message);
                setTimeout(function () {
                    turnOffLocalCamera();
                    muteLocalMic();
                    sendNotificationEvent('JOINED_THE_ROOM', username);
                    $("#preloader").delay(500).fadeOut("slow");
                }, 3000);

            } else {
                console.log(data.message);
            }
        })
}
function getMeeting() {

    return new Promise((resolve, reject) => {

        $.get(`/v1/video/getMeeting/${meetingCode}`,
            function (data, status) {
                if (data.status == 200) {

                    //console.log("\n \n \n getMeeting >>  " + JSON.stringify(data))
                    var meetingData = data.data;
                    resolve(meetingData);

                } else {
                    console.log("\n unable to get meeting")
                    //reject()
                }
            });
    })
}