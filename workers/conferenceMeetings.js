const { v1: uuidv1 } = require('uuid');
const Meeting = require('../app/models/conferenceMeeting');

class MeetingCls {

    MeetingCls() { }

    save(params) {

        const meetingDocument = new Meeting({
            meetingCode: `${params.name}_${uuidv1()}`,
            name: params.name,
            host: params.host,
            participants: [],
        });
        return new Promise((resolve, reject) => {
            meetingDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Meeting Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }


    findOne(params) {
        return new Promise((resolve, reject) => {
            Meeting.findOne({
                meetingCode: params.meetingCode,
            }, (err, meetingData) => {
                if (err) {
                    console.error(`Error :: findOne Meeting Error :: ${JSON.stringify(err)}`);
                    reject(err)
                } else {
                    resolve(meetingData)
                }
            })
        })
    }


    findall() {
        return new Promise(async (resolve, reject) => {

            Meeting.find({})
                .sort({ _id: -1 })
                .exec((err, Meeting) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all Meeting has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log(`Meeting LENGTH> ${Meeting.length}`);
                        resolve(Meeting);
                    }
                });
        });
    }


    addParticipant(params) {
        return new Promise((resolve, reject) => {

            var participant = {
                email: params.email,
                name: params.name,
                streamId: params.streamId
            }

            Meeting.findOne({ meetingCode: params.meetingCode }, { participants: { $elemMatch: { email: participant.email } } },
                (err, response) => {
                    if (err) {
                        console.log(`err in findOne Meeting: ${util.inspect(err)}`);
                        reject(err);
                    } else {
                        //console.log("\n \n response >>>> " + JSON.stringify(response))
                        if (response.participants) {
                            Meeting.updateOne({
                                "meetingCode": params.meetingCode,
                                "participants.email": participant.email
                            }, {
                                $set: {
                                    "participants.$.streamId": participant.streamId
                                }
                            }, { multi: true },
                                (err, meetingData) => {
                                    if (err) {
                                        console.log(`err in Update Meeting: ${util.inspect(err)}`);
                                        reject(err);
                                    } else {
                                        resolve(meetingData);
                                    }
                                });

                        } else {

                            Meeting.updateOne(
                                { meetingCode: params.meetingCode },
                                { $push: { participants: participant } },
                                (err, meetingData) => {
                                    if (err) {
                                        console.log(`err in Update Meeting: ${util.inspect(err)}`);
                                        reject(err);
                                    } else {
                                        resolve(meetingData);
                                    }
                                });
                        }
                    }
                })
        })
    }


    getParticipant(params) {
        return new Promise((resolve, reject) => {
            Meeting.findOne({ meetingCode: params.meetingCode }, { participants: { $elemMatch: { streamId: params.streamId } } },
                (err, response) => {
                    if (err) {
                        console.log(`err in findOne Meeting: ${util.inspect(err)}`);
                        reject(err);
                    } else {
                        if (response.participants) {
                            resolve(response.participants);
                        } else {
                            reject()
                        }
                    }
                })
        })
    }

}


module.exports = {
    MeetingClass: MeetingCls,
};